pragma solidity ^0.5.0;

contract CarTechnicalReview {

    address owner;

    bytes17[] public vins;
    mapping(bytes17 => Vehicle) public vehicles;

    mapping(bytes17 => address) public vehicleOwners;

    address[] public accounts;
    mapping(address => User) public users;

    mapping(uint => Defect) public defects;
    uint private nextDefectId = 0;

    constructor() public {
        owner = msg.sender;
    }

	enum Role { 
      	mechanic, 
      	technician, 
        police 
    }
  
  	struct User {
      	address pubkey;
      	bytes16 name;
        Role role;
    }
  
    struct Defect {
        uint id;
        bytes16 name;
        bytes32 description;
        bool serious;
        uint whenReported;
        address whoReported;
    }

	struct Vehicle {
		bytes17 vin;
      	string brand;
      	string model;
      	uint mileage;
        uint[] smallDefects;
        uint[] seriousDefects;
    }

    event CrashAccident (
        bytes17 indexed _vin,
        uint indexed _id
    );

    modifier onlyOwner {
        require(
            msg.sender == owner,
            "Only owner can call this function."
        );
        _;
    }

    modifier authoritative {
        require(
            users[msg.sender].role == Role.mechanic || users[msg.sender].role == Role.technician,
            "Only mechanics and technicians can call this function."
        );
        _;
    }

    modifier superauthoritative {
        require(
            users[msg.sender].role == Role.police,
            "Only police can call this function."
        );
        _;
    }

    modifier oneMoreDefect {
        _;
        nextDefectId++;
    }

    function getVins() public view returns (bytes17[] memory) {
        return vins;
    }

    function getAccounts() public view returns (address[] memory) {
        return accounts;
    }

    function getSmallDefects(bytes17 vin) public view returns (uint[] memory) {
        return vehicles[vin].smallDefects;
    }

    function getSeriousDefects(bytes17 vin) public view returns (uint[] memory) {
        return vehicles[vin].seriousDefects;
    }

	function addUser(address _pubkey, bytes16 _name, Role _role) public onlyOwner {
        User memory user = User(_pubkey, _name, _role);
        users[_pubkey] = user;
        accounts.push(_pubkey);
    }

    function addVehicle(bytes17 _vin, string memory _brand, string memory _model, uint _mileage, address vehicleOwner) public authoritative {
        Vehicle memory vehicle = Vehicle(_vin, _brand, _model, _mileage, new uint[](0), new uint[](0));
        vehicles[_vin] = vehicle;
        vehicleOwners[_vin] = vehicleOwner;
        vins.push(_vin);
    }

    function addDefect(bytes16 _name, bytes32 _description, uint[] storage _targetDefects, bool _serious) internal oneMoreDefect {
        Defect memory defect = Defect(nextDefectId, _name, _description, _serious, now, msg.sender);
        defects[nextDefectId] = defect;
        _targetDefects.push(nextDefectId);
    }

    function addDefects(bytes16[] memory _names, bytes32[] memory _descriptions, uint[] storage _targetDefects, bool _serious) internal {
        for (uint i=0; i<_names.length; i++) {
            addDefect(_names[i], _descriptions[i], _targetDefects, _serious);
        }
    }

    function moveDefects(uint[] storage _targetDefects, uint[] storage _sourceDefects) internal {
        for (uint i=0; i<_sourceDefects.length; i++) {
            _targetDefects.push(_sourceDefects[i]);
        }
        _sourceDefects.length = 0;
    }

    function removeDefects(uint[] memory _whichDefects, uint[] storage _sourceDefects) internal {
        for (uint i = 0; i<_whichDefects.length; i++) {
            uint sourceDefectsLength = _sourceDefects.length;
            uint removedId;
            for (uint j = 0; j<sourceDefectsLength; j++) {
                if (_sourceDefects[j] == _whichDefects[i]) {
                    removedId = j;
                    break;
                }
            }
            for (uint j=removedId; j<sourceDefectsLength-1; j++) {
                _sourceDefects[j] = _sourceDefects[j+1];
            }
            delete _sourceDefects[sourceDefectsLength-1];
            _sourceDefects.length--;
        }
    }

    function review(bytes17 _vin, uint _mileage, bytes16[] memory _names, bytes32[] memory _descriptions, uint[] memory _fixedDefects, bytes16[] memory _permanentNames, bytes32[] memory _permanentDescriptions) public authoritative {
        Vehicle storage vehicle = vehicles[_vin];
        require(
            _mileage >= vehicle.mileage,
            "Car mileage is irremovable."
        );
        vehicle.mileage = _mileage;
        removeDefects(_fixedDefects, vehicle.smallDefects);
        //moveDefects(vehicle.seriousDefects, vehicle.smallDefects);
        addDefects(_names, _descriptions, vehicle.smallDefects, false);
        addDefects(_permanentNames, _permanentDescriptions, vehicle.seriousDefects, true);
    }

    function registerCrashEvent(bytes17 _vin, bytes16 _name, bytes32 _description) public superauthoritative {
        Vehicle storage vehicle = vehicles[_vin];
        addDefect(_name, _description, vehicle.seriousDefects, true);
        emit CrashAccident(_vin, nextDefectId);
    }
}
