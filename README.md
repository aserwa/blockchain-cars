# Car Technical Review

## How to run

- install truffle and ganache
- ganache -> quickstart
- in ganache: settings  → add project → select `truffle-config.js` then  save & restart

run `chmod +x run_me.sh && ./run_me.sh` .  It would run commands below:
- `truffle compile`
- `truffle migrate` 
- `yarn` in root directory to install dependencies
- `cd client`
- `yarn` to install react and dependencies
- `yarn start` to run app

then
- ganache → contracts should be deployed
- first address in ganache is set as Contract Owner. Use mnemonic in ganache as login credentials.  
- in metamask select network `HTTP://127.0.0.1:7545`
- go to `127.0.0.1:3000` in browser, application to load may take a while
- metamask will ask about contract, accept it
- use application


## Contract tests

- `cd client/src`
- `yarn test-api`

## Contributors
- Angelika Serwa
- Jerzy Skrzypek
- Krzysiek Romanowski
- Paweł Rozwoda


![alt text](img/0_add_new_user.png "add user")
![alt text](img/1_add_vehicle.png "add vehicle")
![alt text](img/2_vehicle_added.png "vehicle added")
![alt text](img/3_see_cars.png "see cars")
![alt text](img/4_add_review.png "add review")
![alt text](img/5_confirm_review_metamas.png "metamask confirmation")
![alt text](img/6_review_added.png "review added")
![alt text](img/ganache_accounts.png "ganache accounts")
