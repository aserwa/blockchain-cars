import React from "react";
import styled from "styled-components";
import { Paper, Box } from "@material-ui/core";

const Container = styled(Box)`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: stretch;
`;

const Card = ({ children }) => {
  return (
    <Container>
      <Paper>{children}</Paper>
    </Container>
  );
};

export default Card;
