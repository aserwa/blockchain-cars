import React, { useEffect, useState } from "react";
import Card from "./Card";
import SearchBar from "./SearchBar";
import Table from "./Table";
import { Divider, Box } from "@material-ui/core";
import CarDetails from "./CarDetails";
import { useApi } from "../api/useApi";
import { searchCar as searchCarApi } from "../api/api";

const SearchCars = ({ selectedCar, setSelectedCar }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [search, isSearching, data] = useApi(() => searchCarApi(searchTerm));
  useEffect(() => {
    search();
  }, [searchTerm]);

  const tableData = (data || []).map(car => [car.VIN, car.brand, car.owner]);

  return (
    <div>
      <Card>
        <SearchBar
          loading={isSearching}
          value={searchTerm}
          onChange={txt => setSearchTerm(txt)}
        />
        <Divider />
        <Table
          columns={["VIN", "Brand", "Owner"]}
          data={tableData}
          onRowPress={i => setSelectedCar(data[i])}
        />
      </Card>
      <Box mt={2} />
      {selectedCar && <CarDetails details={selectedCar} />}
    </div>
  );
};

export default SearchCars;
