import React from "react";
import SearchIcon from "@material-ui/icons/Search";
import { InputBase, Box, CircularProgress } from "@material-ui/core";
import styled from "styled-components";

const Container = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #f5f5f5;
`;

const TextInput = styled(InputBase)`
  flex: 1;
`;

const SearchBar = ({ loading, value, onChange }) => (
  <Container p={2}>
    <SearchIcon />
    <Box ml={1} />
    <TextInput
      placeholder="Search..."
      value={value}
      onChange={e => onChange(e.target.value)}
    />
    {loading && <CircularProgress size={24} />}
  </Container>
);

export default SearchBar;
