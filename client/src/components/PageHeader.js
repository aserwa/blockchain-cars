import React from "react";
import { Typography, AppBar, Toolbar } from "@material-ui/core";

const PageHeader = ({ title }) => {
  return (
    <AppBar position="sticky">
      <Toolbar>
        <Typography variant="h6" noWrap>
          {title}
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default PageHeader;
