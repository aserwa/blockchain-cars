import React, { useState } from "react";
import clsx from "clsx";
import { Snackbar, SnackbarContent, IconButton } from "@material-ui/core";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";
import CloseIcon from "@material-ui/icons/Close";
import { green } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";

export const SnackbarContext = React.createContext({});

export const SnackbarVariants = {
  ERROR: "ERROR",
  SUCCESS: "SUCCESS",
  INFO: "INFO"
};

const variantIcon = {
  [SnackbarVariants.SUCCESS]: CheckCircleIcon,
  [SnackbarVariants.ERROR]: ErrorIcon,
  [SnackbarVariants.INFO]: InfoIcon
};

const useStyles1 = makeStyles(theme => ({
  [SnackbarVariants.SUCCESS]: {
    backgroundColor: green[600]
  },
  [SnackbarVariants.ERROR]: {
    backgroundColor: theme.palette.error.dark
  },
  [SnackbarVariants.INFO]: {
    backgroundColor: theme.palette.primary.main
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));

export const SnackbarProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [variant, setVariant] = useState(SnackbarVariants.INFO);
  const classes = useStyles1();
  const Icon = variantIcon[variant];

  return (
    <SnackbarContext.Provider
      value={{
        openSnackbar: () => setOpen(true),
        setSnackbarMessage: setMessage,
        setSnackbarVariant: setVariant
      }}
    >
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
      >
        <SnackbarContent
          className={classes[variant]}
          message={
            <span className={classes.message}>
              <Icon className={clsx(classes.icon, classes.iconVariant)} />
              {message}
            </span>
          }
          action={[
            <IconButton key="close" onClick={() => setOpen(false)}>
              <CloseIcon className={classes.icon} />
            </IconButton>
          ]}
        />
      </Snackbar>
      {children}
    </SnackbarContext.Provider>
  );
};
