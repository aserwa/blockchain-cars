import React from "react";
import {
  Drawer as MUIDrawer,
  List,
  ListItem,
  ListItemText,
  ListSubheader
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import styled from "styled-components";
import { Link, withRouter } from "react-router-dom";

const drawerWidth = 250;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    height: "100vh"
  },
  drawerPaper: {
    width: drawerWidth
  }
}));

const StyledLink = styled(Link)`
  text-decoration: none;
  color: inherit;
`;

const Drawer = ({ sections, location: { pathname } }) => {
  const classes = useStyles();
  return (
    <MUIDrawer
      variant="permanent"
      anchor="left"
      className={classes.drawer}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      {sections.map(({ title, items }) => (
        <List
          key={title}
          subheader={<ListSubheader disableSticky>{title}</ListSubheader>}
        >
          {items.map(item => (
            <StyledLink to={item.path} key={item.path}>
              <ListItem button selected={pathname === item.path}>
                <ListItemText primary={item.title} />
              </ListItem>
            </StyledLink>
          ))}
        </List>
      ))}
    </MUIDrawer>
  );
};

export default withRouter(Drawer);
