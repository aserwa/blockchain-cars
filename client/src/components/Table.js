import React from "react";
import {
  TableHead as MUITableHead,
  TableRow,
  TableCell,
  Table as MUITable,
  TableBody
} from "@material-ui/core";
import styled from "styled-components";

const TableHead = styled(MUITableHead)`
  background-color: #f5f5f5;
`;

const Table = ({ columns, data, onRowPress }) => {
  return (
    <MUITable>
      <TableHead>
        <TableRow>
          {columns.map(column => (
            <TableCell key={column}>{column}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map((row, i) => (
          <TableRow
            key={i}
            hover={!!onRowPress}
            onClick={() => onRowPress && onRowPress(i)}
          >
            {row.map((cell, i) => (
              <TableCell key={i}>{cell}</TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </MUITable>
  );
};

export default Table;
