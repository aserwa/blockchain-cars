import React from "react";
import styled from "styled-components";
import Card from "./Card";
import { List, ListItemText, ListItem, ListSubheader } from "@material-ui/core";

const Columns = styled.div`
  display: flex;
`;

const Flex = styled.div`
  display: flex;
  flex: 1;
`;

const CarDetails = ({
  details: {
    VIN,
    brand,
    model,
    owner,
    mileage,
    temporaryDefects,
    permanentDefects
  }
}) => (
  <Card>
    <Columns>
      <Flex>
        <List>
          <ListSubheader>VIN number</ListSubheader>
          <ListItem>
            <ListItemText primary={VIN} />
          </ListItem>
          <ListSubheader>brand</ListSubheader>
          <ListItem>
            <ListItemText primary={brand} />
          </ListItem>
          <ListSubheader>model</ListSubheader>
          <ListItem>
            <ListItemText primary={model} />
          </ListItem>
        </List>
      </Flex>
      <Flex>
        <List>
          <ListSubheader>owner</ListSubheader>
          <ListItem>
            <ListItemText primary={owner} />
          </ListItem>
          <ListSubheader>mileage</ListSubheader>
          <ListItem>
            <ListItemText primary={`${mileage} km`} />
          </ListItem>
        </List>
      </Flex>
      <Flex>
        <List>
          <ListSubheader>temporary defects</ListSubheader>
          {temporaryDefects.map(({ name, description }) => (
            <ListItem key={name}>
              <ListItemText primary={name} secondary={description} />
            </ListItem>
          ))}
        </List>
      </Flex>
      <Flex>
        <List>
          <ListSubheader>permanent defects</ListSubheader>
          {permanentDefects.map(({ name, description }) => (
            <ListItem key={name}>
              <ListItemText primary={name} secondary={description} />
            </ListItem>
          ))}
        </List>
      </Flex>
    </Columns>
  </Card>
);

export default CarDetails;
