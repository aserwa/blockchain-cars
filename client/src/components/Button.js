import React from "react";
import { Button as MUIButton, CircularProgress } from "@material-ui/core";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  position: relative;
`;

const Progress = styled(CircularProgress)`
  position: absolute;
  margin: auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

const Button = ({ children, onPress, loading }) => (
  <Container>
    <MUIButton
      variant="contained"
      color="primary"
      onClick={onPress}
      disabled={loading}
    >
      {children}
    </MUIButton>
    {loading && <Progress size={24} />}
  </Container>
);

export default Button;
