import React from "react";
import { Card } from "@material-ui/core";

const ScreenCard = ({ children }) => <Card>{children}</Card>;

export default ScreenCard;
