import React from "react";
import { Box } from "@material-ui/core";
import styled from "styled-components";

const Form = styled(Box)`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: stretch;
  max-width: 500px;
  margin: auto;
`;

export default ({ children }) => <Form p={4}>{children}</Form>;
