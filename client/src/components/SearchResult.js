import React from "react";
import { Paper } from "@material-ui/core";

const SearchResult = ({ children }) => <Paper>{children}</Paper>;

export default SearchResult;
