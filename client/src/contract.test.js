//Taken from: https://gitlab.com/tomasz.rozmus/blockchain-practicum/tree/master/packages/07_local_smart_contracts/test/Faucet.test.js
const ganache = require('ganache-cli');
const Web3 = require('web3');
const provider = ganache.provider();
provider.setMaxListeners(18);
const web3 = new Web3(provider);// here we provide our virtual, ganache provider.
const contract = require('./contracts/CarTechnicalReview');
const contractInterface = contract['abi'];
const contractBytecode = contract['bytecode'];
jest.setTimeout(30000);

let accounts;
let carTechnicalReview;
let deployer;

beforeEach(async () => {
	// Get a list of all accounts
	accounts = await web3.eth.getAccounts();

	// The Contract
	// create contract instance based on Application Binary Interface (ABI)
	const contractInstance = new web3.eth.Contract(contractInterface);
	// deploy the contract passing initial value to constructor
	const contractDeployed = contractInstance.deploy({
		data: contractBytecode,
		arguments: []
	});
	deployer = accounts[9];
	await contractDeployed.send({ from: deployer, gas: '5000000' }).then((instance) => {
		console.log("Contract mined at " + instance.options.address);
		carTechnicalReview = instance;
	});
});

describe('carTechnicalReview', () => {

	it('one big scenario', async () => {
		let mechanic = accounts[0];
		const vehicleOwner  = accounts[1];
		const vin = "1FMYU02ZX5DA03812";
		const mileage = 234234;

		await carTechnicalReview.methods.addUser(mechanic, web3.utils.utf8ToHex("Nakamoto"), 0).send({
			from: deployer, gas: '2300000'
		});

		await carTechnicalReview.methods.addVehicle(web3.utils.utf8ToHex(vin), "Suzuki", "Swift", mileage, vehicleOwner).send({
			from: mechanic, gas: '2300000'
		});

		let res = await carTechnicalReview.methods.getVins().call();
		console.log(web3.utils.hexToUtf8(res[0]));

		expect(vin).toEqual(web3.utils.hexToUtf8(res[0]));
		res = await carTechnicalReview.methods.getAccounts().call();
		expect(mechanic).toEqual(res[0]);
		res = await carTechnicalReview.methods.users(res[0]).call();

		let defectsNames = [];
		let defectsDesc = [];
		defectsNames.push(web3.utils.utf8ToHex("Głowica"));
		defectsDesc.push(web3.utils.utf8ToHex("Cieknie"));
		await carTechnicalReview.methods.review(web3.utils.utf8ToHex(vin), mileage+44, defectsNames, defectsDesc, new Array(), new Array(), new Array()).send({
			from: mechanic, gas: '2300000'
		});
		res = await carTechnicalReview.methods.vehicles(web3.utils.utf8ToHex(vin)).call();
	}) 


	it('Add auth users', async() => {

		console.log("accounts")
		console.log(accounts)

		let mechanic = accounts[0];
		await carTechnicalReview.methods.addUser(mechanic, web3.utils.utf8ToHex("Heniu"), 0).send({
			from: deployer, gas: '2300000'
		});

		let technician = accounts[1];
		await carTechnicalReview.methods.addUser(technician, web3.utils.utf8ToHex("Gieniu"), 1).send({
			from: deployer, gas: '2300000'
		});

		let police = accounts[2];
		await carTechnicalReview.methods.addUser(police, web3.utils.utf8ToHex("Konrad"), 2).send({
			from: deployer, gas: '2300000'
		}); 

		res = await carTechnicalReview.methods.getAccounts().call();
		console.log('res')
		console.log(res)
		let mech = await carTechnicalReview.methods.users(res[0]).call();
		let tech = await carTechnicalReview.methods.users(res[1]).call();
		let poli = await carTechnicalReview.methods.users(res[2]).call();

		expect(res.length).toEqual(3)
		expect(mech.role).toEqual("0")
		expect(tech.role).toEqual("1")
		expect(poli.role).toEqual("2")
		

	})

	it('add vehicle', async() => { 
		let mechanic = accounts[0]; 
		const vehicleOwner  = accounts[1];
		const vin = "21FMYU02ZX5DA0381"
		const mileage = 234234;
		await carTechnicalReview.methods.addUser(mechanic, web3.utils.utf8ToHex("satoshi nakamoto"), 0).send({
			from: deployer, gas: '2300000'
		});

		await carTechnicalReview.methods.addVehicle(web3.utils.utf8ToHex(vin), "hyundai", "i30", mileage, vehicleOwner).send({
			from: mechanic, gas: '2300000'
		});


		let res = await carTechnicalReview.methods.getVins().call();

		expect(vin).toEqual(web3.utils.hexToUtf8(res[0]));
	})

	
	it('increase mileage', async() => { 

		let mechanic = accounts[0];
		const vehicleOwner  = accounts[3];
		const vin = "1FMYU02ZX5DA03812";
		const mileage = 101;
		await carTechnicalReview.methods.addUser(mechanic, web3.utils.utf8ToHex("Katsumoto"), 0).send({
			from: deployer, gas: '2300000'
		});
		await carTechnicalReview.methods.addVehicle(web3.utils.utf8ToHex(vin), "Ford", "Escort", mileage, vehicleOwner).send({
			from: mechanic, gas: '2300000'
		});

		let defectsNames = [];
		let defectsDesc = [];
		await carTechnicalReview.methods.review(web3.utils.utf8ToHex(vin), mileage+44, defectsNames, defectsDesc, new Array(), new Array(), new Array()).send({
			from: mechanic, gas: '2300000'
		});
		res = await carTechnicalReview.methods.vehicles(web3.utils.utf8ToHex(vin)).call();
		console.log('vehicle data')
		console.log(res)
		expect(res.mileage).toEqual("145") 
	}) 
});
