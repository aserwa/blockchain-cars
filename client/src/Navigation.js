import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import styled from "styled-components";
import PageHeader from "./components/PageHeader";
import Drawer from "./components/Drawer";
import AddAuthUserScreen from "./screens/AddAuthUserScreen";
import SearchScreen from "./screens/SearchScreen";
import AddVehicleScreen from "./screens/AddVehicleScreen";
import AddReviewScreen from "./screens/AddReviewScreen";
import AddCrashEventScreen from "./screens/AddCrashEventScreen";
import SearchUsersScreen from "./screens/SearchUsersScreen";
import { Box } from "@material-ui/core";

const routes = [
  {
    title: "Contract owner",
    items: [
      {
        title: "Add authoritative user",
        path: "/add_auth_user",
        component: AddAuthUserScreen
      }
    ]
  },
  {
    title: "Authoritative user",
    items: [
      {
        title: "Add vehicle",
        path: "/add_vehicle",
        component: AddVehicleScreen
      },
      {
        title: "Add review",
        path: "/add_review",
        component: AddReviewScreen
      }
    ]
  },
  {
    title: "Super authoritative user",
    items: [
      {
        title: "Add crash event",
        path: "/add_crash_event",
        component: AddCrashEventScreen
      }
    ]
  },
  {
    title: "User",
    items: [
      {
        title: "See cars",
        path: "/cars",
        component: SearchScreen
      },
      {
        title: "See users",
        path: "/users",
        component: SearchUsersScreen
      }
    ]
  }
];

const Root = styled.div`
  display: flex;
`;

const Content = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Navigation = () => {
  return (
    <Router>
      <Root>
        <Drawer sections={routes} />
        {routes.map(({ items }) =>
          items.map(link => {
            const Screen = link.component;
            return (
              <Route
                key={link.path}
                path={link.path}
                exact
                render={() => (
                  <Content>
                    <PageHeader title={link.title} />
                    <Box px={8} py={2}>
                      <Screen />
                    </Box>
                  </Content>
                )}
              />
            );
          })
        )}
      </Root>
    </Router>
  );
};

export default Navigation;
