import React, { useState } from "react";
import Card from "../components/Card";
import Form from "../components/Form";
import Button from "../components/Button";
import { TextField, Box } from "@material-ui/core";
import SearchCars from "../components/SearchCars";
import { useApi } from "../api/useApi";
import { addCrashEvent as addCrashEventApi } from "../api/api";

const AddCrashEventScreen = () => {
  const [selectedCar, setSelectedCar] = useState(null);
  const [description, setDescription] = useState("");
  const [name, setName] = useState("");
  const [addCrashEvent, isAddingCrashEvent] = useApi(
    () =>
      addCrashEventApi({
        VIN: selectedCar && selectedCar.VIN,
        description,
        name
      }),
    "Crash event was added successfully"
  );

  return (
    <div>
      <h1>Find the car</h1>
      <SearchCars selectedCar={selectedCar} setSelectedCar={setSelectedCar} />
      {selectedCar && (
        <>
          <h1>Describe crash event</h1>
          <Card>
            <Form>
              <TextField
                label="name"
                multiline
                value={name}
                onChange={e => setName(e.target.value)}
              />
              <Box mt={2} />
              <TextField
                label="description"
                multiline
                value={description}
                onChange={e => setDescription(e.target.value)}
              />
              <Box alignSelf="center" mt={2}>
                <Button loading={isAddingCrashEvent} onPress={addCrashEvent}>
                  Add crash event
                </Button>
              </Box>
            </Form>
          </Card>
        </>
      )}
    </div>
  );
};

export default AddCrashEventScreen;
