import React, { useState, useEffect } from "react";
import Card from "../components/Card";
import SearchBar from "../components/SearchBar";
import Table from "../components/Table";
import { Divider } from "@material-ui/core";
import { useApi } from "../api/useApi";
import { searchUser as searchUserApi } from "../api/api";

const SearchUsersScreen = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [search, isSearching, data] = useApi(() => searchUserApi(searchTerm));
  useEffect(() => {
    search();
  }, [searchTerm]);

  const tableData = (data || []).map(user => [
    user.address,
    user.name,
    user.role
  ]);

  return (
    <Card>
      <SearchBar
        loading={isSearching}
        value={searchTerm}
        onChange={txt => setSearchTerm(txt)}
      />
      <Divider />
      <Table columns={["Wallet number", "Name", "Role"]} data={tableData} />
    </Card>
  );
};

export default SearchUsersScreen;
