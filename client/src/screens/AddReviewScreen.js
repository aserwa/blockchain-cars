import React, { useState } from "react";
import Card from "../components/Card";
import Form from "../components/Form";
import Button from "../components/Button";
import {
  TextField,
  Box,
  List,
  ListItem,
  ListItemIcon,
  Checkbox,
  ListItemText,
  ListItemSecondaryAction,
  IconButton
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import SearchCars from "../components/SearchCars";
import { useApi } from "../api/useApi";
import { addReview as addReviewApi } from "../api/api";

const AddReviewScreen = () => {
  const [newDefects, setNewDefects] = useState([]);
  const [selectedCar, setSelectedCar] = useState(null);
  const [mileage, setMileage] = useState(0);
  const [fixedDefects, setFixedDefects] = useState({});
  const [addReview, isAddingReview] = useApi(
    () =>
      addReviewApi({
        VIN: selectedCar && selectedCar.VIN,
        mileage,
        temporaryDefects: newDefects
          .filter(defect => !defect.permanent)
          .map(({ name, description }) => ({ name, description })),
        permanentDefects: newDefects
          .filter(defect => defect.permanent)
          .map(({ name, description }) => ({ name, description })),
        fixedDefects: Object.entries(fixedDefects)
          .filter(([k, v]) => v)
          .map(([k, v]) => k)
      }),
    "Review added successfully"
  );

  const updateDefect = (i, def) => {
    const defects = [...newDefects];
    defects[i] = { ...defects[i], ...def };
    setNewDefects(defects);
  };

  const deleteDefect = i => {
    const defects = [...newDefects];
    defects.splice(i, 1);
    setNewDefects(defects);
  };

  return (
    <div>
      <h1>Find the car</h1>
      <SearchCars selectedCar={selectedCar} setSelectedCar={setSelectedCar} />
      {selectedCar && (
        <>
          <h1>Add review details</h1>
          <Card>
            <Form>
              <TextField
                type="number"
                label="mileage"
                helperText="km"
                value={mileage}
                onChange={e => setMileage(e.target.value)}
              />
              <Box mt={2} />
              <h2>Previous temporary defects</h2>
              <h4>Check fixed defects</h4>
              <List>
                {selectedCar.temporaryDefects.map(
                  ({ name, description, id }) => (
                    <ListItem key={id}>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={!!fixedDefects[id]}
                          onChange={e =>
                            setFixedDefects({
                              ...fixedDefects,
                              [id]: e.target.checked
                            })
                          }
                        />
                      </ListItemIcon>
                      <ListItemText primary={name} secondary={description} />
                    </ListItem>
                  )
                )}
              </List>
              <h2> New defects</h2>
              <h4>Permanent?</h4>
              <List>
                {newDefects.map(({ name, description, permanent }, i) => (
                  <ListItem key={i}>
                    <ListItemIcon>
                      <Checkbox
                        edge="start"
                        checked={permanent}
                        onChange={e =>
                          updateDefect(i, { permanent: e.target.checked })
                        }
                      />
                    </ListItemIcon>
                    <div>
                      <TextField
                        value={name}
                        label="Name"
                        fullWidth
                        onChange={e =>
                          updateDefect(i, { name: e.target.value })
                        }
                      />
                      <Box mt={1} />
                      <TextField
                        value={description}
                        multiline
                        onChange={e =>
                          updateDefect(i, { description: e.target.value })
                        }
                        label="Description"
                        fullWidth
                      />
                    </div>
                    <ListItemSecondaryAction>
                      <IconButton
                        edge="end"
                        aria-label="Delete"
                        onClick={() => deleteDefect(i)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}
              </List>
              <Box alignSelf="flex-end">
                <Button
                  onPress={() =>
                    setNewDefects([
                      ...newDefects,
                      { name: "", description: "", permanent: false }
                    ])
                  }
                >
                  +
                </Button>
              </Box>
              <Box alignSelf="center" mt={2}>
                <Button onPress={addReview} loading={isAddingReview}>
                  Add review
                </Button>
              </Box>
            </Form>
          </Card>
        </>
      )}
    </div>
  );
};

export default AddReviewScreen;
