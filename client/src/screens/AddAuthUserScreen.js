import React, { useState } from "react";
import {
  TextField,
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select
} from "@material-ui/core";
import Card from "../components/Card";
import Button from "../components/Button";
import Form from "../components/Form";
import { useApi } from "../api/useApi";
import { addAuthUser as addUserApi } from "../api/api";

const userRoles = {
  POLICE: "Police",
  MECHANIC: "Mechanic",
  TECHNICIAN: "Technician"
};

const AddAuthUserScreen = () => {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [role, setRole] = useState(userRoles.POLICE);

  const [addUser, isAddingUser] = useApi(
    () => addUserApi({ name, address, role }),
    "User was added"
  );
  return (
    <div>
      <h1>Enter user details</h1>
      <Card>
        <Form>
          <TextField
            label="Name"
            value={name}
            onChange={e => setName(e.target.value)}
          />
          <Box mt={2} />
          <TextField
            label="Address"
            value={address}
            onChange={e => setAddress(e.target.value)}
          />
          <Box mt={2} />
          <FormControl>
            <InputLabel>User type</InputLabel>
            <Select value={role} onChange={e => setRole(e.target.value)}>
              {Object.values(userRoles).map(v => (
                <MenuItem value={v} key={v}>
                  {v}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Box alignSelf="center" mt={2}>
            <Button loading={isAddingUser} onPress={addUser}>
              Add user
            </Button>
          </Box>
        </Form>
      </Card>
    </div>
  );
};

export default AddAuthUserScreen;
