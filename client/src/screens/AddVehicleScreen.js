import React, { useState } from "react";
import Card from "../components/Card";
import { TextField, Box } from "@material-ui/core";
import Button from "../components/Button";
import Form from "../components/Form";
import { useApi } from "../api/useApi";
import { addVehicle as addVehicleApi } from "../api/api";

const AddVehicleScreen = () => {
  const [VIN, setVIN] = useState("");
  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [owner, setOwner] = useState("");
  const [mileage, setMileage] = useState(0);

  const [addVehicle, isAddingVehicle] = useApi(
    () => addVehicleApi({ VIN, brand, model, mileage, owner }),
    "Vehicle was added successfully"
  );

  return (
    <div>
      <h1>Enter vehicle details</h1>
      <Card>
        <Form>
          <TextField
            label="VIN"
            value={VIN}
            onChange={e => setVIN(e.target.value)}
          />
          <Box mt={2} />
          <TextField
            label="Brand"
            value={brand}
            onChange={e => setBrand(e.target.value)}
          />
          <Box mt={2} />
          <TextField
            label="Model"
            value={model}
            onChange={e => setModel(e.target.value)}
          />
          <Box mt={2} />
          <TextField
            label="Owner"
            value={owner}
            onChange={e => setOwner(e.target.value)}
          />
          <Box mt={2} />
          <TextField
            label="Mileage"
            value={mileage}
            onChange={e => setMileage(e.target.value)}
            type="number"
            helperText="km"
          />
          <Box mt={4} />
          <Box alignSelf="center" mt={2}>
            <Button onPress={addVehicle} loading={isAddingVehicle}>
              Add vehicle
            </Button>
          </Box>
        </Form>
      </Card>
    </div>
  );
};

export default AddVehicleScreen;
