import React, { useState } from "react";
import SearchCars from "../components/SearchCars";

const SearchScreen = () => {
  const [selectedCar, setSelectedCar] = useState(null);
  return (
    <SearchCars selectedCar={selectedCar} setSelectedCar={setSelectedCar} />
  );
};

export default SearchScreen;
