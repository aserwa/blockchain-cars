import getWeb3 from "../utils/getWeb3";
import { contractAddress } from "../address.js";
//blockchain connectors
const Web3 = require('web3');
const metadata = require('../contracts/CarTechnicalReview');
const network = "http://127.0.0.1:7545";

async function SetAccounts(){
  BlockchainConnector.accounts = await BlockchainConnector.web3.eth.getAccounts();
}

async function InitWeb3(customProvider){
  if(customProvider != null){
    BlockchainConnector.web3 = await new Web3(customProvider);
    console.log("Using custom provider.");
  } else {
    BlockchainConnector.web3 = await getWeb3();
  }
  await SetAccounts();
  console.log("Contract variables set.");
  return await new BlockchainConnector.web3.eth.Contract(metadata.abi, contractAddress);
};

class BlockchainConnector {
  static GetContract() {
    return this.contract;
  }
}
BlockchainConnector.contract = InitWeb3(null);

const Roles = {
  MECHANIC: "Mechanic",
  TECHNICIAN: "Technician",
  POLICE: "Police",
  vals: {
    "Mechanic": {num: 0},
    "Technician": {num: 1},
    "Police": {num: 2}
  },
  names: {
    0: "Mechanic",
    1: "Technician",
    2: "Police"
  }
};

export const addAuthUser = async ({ address, name, role }) => {
  //truffle deploys contracts with owner accounts[0].
  //Provider is switched from browser injected web3 to generic RPC network to perform this call as owner.
  let currentProvider = await BlockchainConnector.web3.currentProvider;
  try {
    console.log(BlockchainConnector.accounts[0] + " a");

    let contract = await InitWeb3(new Web3.providers.HttpProvider(network));
    console.log(await BlockchainConnector.accounts[0] + " b");
    await contract.methods.addUser(address, BlockchainConnector.web3.utils.utf8ToHex(name), Roles.vals[role].num).send({
      from: BlockchainConnector.accounts[0], gas: '2300000'
    });
    BlockchainConnector.web3.eth.sendTransaction({from:BlockchainConnector.accounts[1], to:address, value:BlockchainConnector.web3.utils.toWei("7.4", "ether")});
  }
  finally {
    BlockchainConnector.contract  = await InitWeb3(currentProvider);
    console.log(BlockchainConnector.accounts[0] + " c");
  }
  console.log("Added User.");
};

export const addVehicle = async ({ VIN, brand, model, mileage, owner }) => {
  console.log(BlockchainConnector.accounts[0]);
  try {
    let contract = await BlockchainConnector.GetContract();
    await contract.methods.addVehicle(BlockchainConnector.web3.utils.utf8ToHex(VIN), brand, model, mileage, owner).send({
      from: BlockchainConnector.accounts[0], gas: '2300000'
    });
  }
  finally {}
  console.log("Added vehicle.");
};

export const addReview = async ({
  VIN,
  mileage,
  temporaryDefects,
  permanentDefects,
  fixedDefects
}) => {
  let contract = await BlockchainConnector.GetContract();
  let mutableDefectsNames = [];
  let mutableDefectsDescription = [];
  for (const defect of temporaryDefects){
    mutableDefectsNames.push(BlockchainConnector.web3.utils.utf8ToHex(defect.name));
    mutableDefectsDescription.push(BlockchainConnector.web3.utils.utf8ToHex(defect.description));
  }
  let permanentDefectsNames = [];
  let permanentDefectsDescription = [];
  for (const defect of permanentDefects){
    permanentDefectsNames.push(BlockchainConnector.web3.utils.utf8ToHex(defect.name));
    permanentDefectsDescription.push(BlockchainConnector.web3.utils.utf8ToHex(defect.description));
  }
  await contract.methods.review(BlockchainConnector.web3.utils.utf8ToHex(VIN), mileage, mutableDefectsNames, mutableDefectsDescription, fixedDefects, permanentDefectsNames, permanentDefectsDescription).send({
    from: BlockchainConnector.accounts[0], gas: '2300000'
  });

};

export const addCrashEvent = async ({ VIN, description, name }) => {
  try {
    let contract = await BlockchainConnector.GetContract();
    await contract.methods.registerCrashEvent(BlockchainConnector.web3.utils.utf8ToHex(VIN),
        BlockchainConnector.web3.utils.utf8ToHex(name), BlockchainConnector.web3.utils.utf8ToHex(description)).send({
      from: BlockchainConnector.accounts[0], gas: '2300000'
    });
  }
  finally {}
  console.log("Added crash event.");
};

export const searchUser = async ({ searchTerm }) => {
  let contract = await BlockchainConnector.GetContract();
  let usersData = new Array();
  let userList = await contract.methods.getAccounts().call();
  for (const user of userList){
    const res = await contract.methods.users(user).call();
    usersData.push({address: res.pubkey, name: BlockchainConnector.web3.utils.hexToUtf8(res.name), role: Roles.names[res.role]});
  }
  return usersData;
};

export const searchCar = async ({ searchTerm }) => {
  let contract = await BlockchainConnector.GetContract();
  let carsData = new Array();
  let carList = await contract.methods.getVins().call();
  for (const car of carList){
    const res = await contract.methods.vehicles(car).call();
    carsData.push({
      VIN: BlockchainConnector.web3.utils.hexToUtf8(res.vin),
      brand: res.brand,
      model: res.model,
      owner: (await contract.methods.vehicleOwners(res.vin).call()),
      mileage: res.mileage,
      temporaryDefects: [],
      permanentDefects: []
    });
    const smallDefects =  await contract.methods.getSmallDefects(res.vin).call();
    for(const defID of smallDefects) {
      const defect = await contract.methods.defects(defID).call();
      carsData[carsData.length - 1].temporaryDefects.push({ id: defect.id, name: BlockchainConnector.web3.utils.hexToUtf8(defect.name),
        description: BlockchainConnector.web3.utils.hexToUtf8(defect.description) });
    }
    const seriousDefects = await contract.methods.getSeriousDefects(res.vin).call();
    for(const defID of seriousDefects) {
      const defect = await contract.methods.defects(defID).call();
      carsData[carsData.length - 1].permanentDefects.push({ id: defect.id, name: BlockchainConnector.web3.utils.hexToUtf8(defect.name),
        description: BlockchainConnector.web3.utils.hexToUtf8(defect.description) });
    }
  }
  return carsData;
};
