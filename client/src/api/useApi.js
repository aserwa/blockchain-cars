import { useState, useContext } from "react";
import {
  SnackbarContext,
  SnackbarVariants
} from "../components/SnackbarContext";

export const useApi = (apiFunction, successMessage) => {
  const [isFetching, setIsFetching] = useState(false);
  const [data, setData] = useState(null);
  const { openSnackbar, setSnackbarMessage, setSnackbarVariant } = useContext(
    SnackbarContext
  );

  const run = async () => {
    try {
      setIsFetching(true);
      const data = await apiFunction();
      if (successMessage) {
        setSnackbarVariant(SnackbarVariants.SUCCESS);
        setSnackbarMessage(successMessage);
        openSnackbar();
      }
      setData(data);
    } catch (error) {
      setSnackbarVariant(SnackbarVariants.ERROR);
      setSnackbarMessage(`ERROR: ${error.message}`);
      openSnackbar();
    } finally {
      setIsFetching(false);
    }
  };

  return [run, isFetching, data];
};
