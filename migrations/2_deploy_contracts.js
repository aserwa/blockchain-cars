const TruffleConfig = require("truffle-config");
const fs = require('fs')
var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var CarTechnicalReview =  artifacts.require("./CarTechnicalReview.sol");

module.exports = async function(deployer) {
  //deployer.deploy(SimpleStorage);
  const config = TruffleConfig.default();
  let address;
  await deployer.deploy(CarTechnicalReview).then(_instance => address =_instance.address);
  fs.writeFile(config.working_directory + "/client/src/address.js", "export const contractAddress = \""+ address +"\";", (err) => {
    if (err) throw err;
  });
  web3.eth.net.getId().then(console.log);
};
